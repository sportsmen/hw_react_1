import React from 'react';
import Header from './sections/Header/index'
import Main from "./sections/Main/index";
import Footer from "./sections/Footer";

function App() {
    return (
        <div className="app">
            <Header />
            <Main />
            <Footer />
        </div>
    );
}

export default App;
