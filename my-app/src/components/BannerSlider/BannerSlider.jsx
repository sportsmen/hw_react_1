import React, {useEffect, useState} from "react";
import PropTypes from 'prop-types';
import Button from "../Button/Button";


const slidesArr = [];


const BannerSlider  = ({numberOfSlides}) => {

    const [slides, setSlides] = useState([]);

    useEffect(() => {
        for(let i = 0; i < numberOfSlides; i++){
            slidesArr.push(i);
        }
        setSlides(slidesArr);
    });

    return(
        <div className='header__middle__slider-box'>
            {slides.map((index) => {
                return(
                    <Button
                        className='header__middle__slider-item'
                        key={index}
                    > </Button>
                )
            })}
        </div>
    )
};


BannerSlider.propTypes = {
    numberOfSlides: PropTypes.number,
};


BannerSlider.defaultProps = {
    numberOfSlides: 0
};

export default BannerSlider;