import React from "react";
import PropTypes from 'prop-types';
import FilmRating from "../FilmRating";
import Genres from "../GenresList";


const FilmCard = ({title,img,genres,rating}) => {
    return(
        <div className='film-card'>
            <div className='film-card__img-box'>
                <img className='film-card__img' alt={title} src={img}/>
            </div>
            <div className='film-card__description'>
                <div className='film-card__description__title-box'>
                    <p className='film-card__title'>{title}</p>
                    <FilmRating
                        className='film-card__rating'
                        rating={rating}
                    />
                </div>
                <Genres className='film-card__genres'>
                    <p>{genres}</p>
                </Genres>
            </div>
        </div>
    )
};


FilmCard.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
    genres: PropTypes.string,
    rating: PropTypes.string,
};


export default FilmCard;