import React from "react";
import Button from "../Button";


const Filters = () => {
    return(
        <div className='film-filters'>
            <Button className='film-filters__item'>Trending</Button>
            <Button className='film-filters__item'>Top Rated</Button>
            <Button className='film-filters__item'>New Arrivals</Button>
            <Button className='film-filters__item'>Trailers</Button>
            <Button className='film-filters__item'>Coming Soon</Button>
            <Button className='film-filters__item'>Genre <i className="fas fa-chevron-down"> </i></Button>
        </div>
    )
};


export default Filters;