import React, {useEffect, useState} from "react";
import FilmCard from "../FilmCard/index";
import Subscribe from "../Subscribe/index";


const FilmList = () => {

    const [filmsList, setFilmsList] = useState([]);

    useEffect(() => {
        fetch('./films.json')
            .then(response => response.json())
            .then(result => setFilmsList(result));
    });

    return(
        <div className='film-list'>
            {filmsList.map((elem, index) => {
                if(index === 12){
                    return (
                        <>
                            <Subscribe/>
                            <FilmCard
                                key={index}
                                {...elem}
                            />
                        </>
                    )
                }
                return(
                    <FilmCard
                        key={index}
                        {...elem}
                    />
                )
            })}
        </div>
    )
};


export default FilmList;