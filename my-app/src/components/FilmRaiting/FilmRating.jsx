import React from "react";
import PropTypes from 'prop-types';


const FilmRating = ({className,rating}) => {
    return(
        <div className={className}>
            <p>{rating}</p>
        </div>
    )
};


FilmRating.propTypes = {
    className: PropTypes.string,
    rating: PropTypes.string,
};


FilmRating.defaultProps = {
    className: 'rating',
    rating: 'undefined',
};


export default FilmRating;