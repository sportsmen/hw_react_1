import React from "react";
import PropTypes from 'prop-types';


const Genres = ({className,children}) => {
    return(
        <div
            className={className}
        >{children}</div>
    )
};


Genres.propTypes = {
    className: PropTypes.string,
};


Genres.defaultProps = {
    children: '',
    className: 'genres-list',
};

export default Genres;