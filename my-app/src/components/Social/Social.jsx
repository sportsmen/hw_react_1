import React from "react";


const Social = () => {
    return(
        <div className='social-box'>
            <i className="fab fa-facebook-f"> </i>
            <i className="fab fa-twitter"> </i>
            <i className="fab fa-pinterest-p"> </i>
            <i className="fab fa-instagram"> </i>
            <i className="fab fa-youtube"> </i>
        </div>
    )
};


export default Social;