import React from "react";
import Button from "../Button";


const Subscribe = () => {
    return(
        <div className='subscribe'>
            <div className='subscribe-backDrop'>
                <p className='subscribe__text'>Receive information on the latest hit movies straight to your inbox.</p>
                <Button className='subscribe__button'>Subscribe!</Button>
            </div>
        </div>
    )
};


export default Subscribe;