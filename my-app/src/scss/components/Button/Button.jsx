import React from "react";
import PropTypes from 'prop-types';


const Button = ({className, onClick, children}) => {
    return(
        <button
            className={className}
            onClick={onClick}
        >{children}</button>
    )
};


Button.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};


Button.defaultProps = {
    className: 'button',
    onClick: () => {},
    children: '',
};


export default Button;

