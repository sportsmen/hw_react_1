import Button from "../Button";
import React from "react";


const ChangeView = () => {
    return(
        <div className='change-view'>
            <Button className='change-view__item'><i className="fas fa-th-large"> </i></Button>
            <Button className='change-view__item'><i className="fas fa-bars"> </i></Button>
        </div>
    )
};


export default ChangeView;