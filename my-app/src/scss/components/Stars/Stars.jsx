import React from "react";


const filmRate = [0,1,2,3,4];


const Stars = () => {
    return(
        <div
            className='header__subBottom__starsBox'
        >
            {filmRate.map((index) => {
                return(
                    <i
                        className="fas fa-star"
                        key={index}
                    > </i>
                )
            })}
        </div>
    )
};


export default Stars;