import React from "react";
import Social from "../../components/Social/index";


const Footer = () => {
    return(
        <div className='footer'>
            <div className='footer__first-box'>
                <div className='footer__links'>
                    <a href="#">About</a>
                    <a href="#">Terms of Service</a>
                    <a href="#">Contact</a>
                </div>
                <a href='#'><h1 className='footer__logo'><span className='footer__logo-subText'>movie</span>rise</h1></a>
                <Social />
            </div>
            <div className='footer__second-box'>
                <p>Copyright © 2017 <span className='footer__second-box__movie'>MOVIE</span>RISE. All Rights Reserved.</p>
            </div>
        </div>
    )
};


export default Footer;