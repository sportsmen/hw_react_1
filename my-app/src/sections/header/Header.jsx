import React from "react";
import Button from "../../components/Button";
import Genres from "../../components/GenresList";
import Stars from "../../components/Stars";
import FilmRating from "../../components/FilmRating";
import BannerSlider from "../../components/BannerSlider";


const Header = () => {
    return(
        <div className='header'>
            <div className='header__top'>
                <a className='header__logo' href=''><h1 className='header__logo'><span className='header__logo-subText'>movie</span>rise</h1></a>
                <div className='header__top__menu-box'>
                    <Button className='header__top__search-button'><i className="fas fa-search"> </i></Button>
                    <Button className='header__top__signIn'>Sign in</Button>
                    <Button className='header__top__signUp'>Sign Up</Button>
                </div>
            </div>
            <div className='header__middle'>
                <BannerSlider
                    numberOfSlides={5}
                />
            </div>
            <div className='header__bottom'>
                <div className='header__bottom__infoBox'>
                    <h2 className='header__bottom__infoBox__h2'>The Jungle Book</h2>
                    <Genres className='header__bottom__infoBox__description'>
                        <p>Adventure</p>
                        <p>Drama</p>
                        <p>Family</p>
                        <p>Fantasy</p>
                        <p>|</p>
                        <p>1h 46m</p>
                    </Genres>
                </div>
            </div>
            <div className='header__subBottom'>
                <div className='header__subBottom__rate-box'>
                    <Stars />
                    <FilmRating
                        className='header__subBottom__rate'
                        rating = '4.8'
                    />
                </div>
                <div className='header__subBottom__actions'>
                    <Button className='header__subBottom__actions__watchNow'>Watch Now</Button>
                    <Button className='header__subBottom__actions__showInfo'>View info</Button>
                    <Button className='header__subBottom__actions__favourites'>+ Favorites</Button>
                    <Button className='header__subBottom__actions__more'><i className="fas fa-ellipsis-v"> </i></Button>
                </div>
            </div>
        </div>
    )
};

export default Header;