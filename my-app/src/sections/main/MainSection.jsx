import React from "react";
import Filters from "../../components/FilmFilters/index";
import ChangeView from "../../components/ChangeView/index";
import FilmList from "../../components/FilmList/index";
import Button from "../../components/Button";


const Main = () => {


    const animation = () => {
        document.getElementById('fountainG').style.display = 'block';
    };


    return(
        <section className='main-section'>
            <div className='main-section__control-panel'>
                <Filters />
                <ChangeView />
            </div>
            <FilmList/>
            <div id="fountainG">
                <div id="fountainG_1" className="fountainG"> </div>
                <div id="fountainG_2" className="fountainG"> </div>
                <div id="fountainG_3" className="fountainG"> </div>
            </div>
            <Button onClick={animation} className='main-section__load-more'>Load more</Button>
        </section>
    )
};


export default Main;